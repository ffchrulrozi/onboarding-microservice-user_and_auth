﻿using Auth.Model;
using Microsoft.EntityFrameworkCore;

namespace Auth
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options) { }
        public DbSet<User> Users { set; get; }
    }
}
