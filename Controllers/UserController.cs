﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Auth.Model;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Auth.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        ApplicationDbContext db;

        public UserController(ApplicationDbContext db)
        {
            this.db = db;
        }
        // GET: api/<UserController>
        [Authorize]
        [HttpGet]
        public object Get()
        {
            return db.Users.Select(x => new { x.ID, x.Username }).ToList();
        }

        // POST api/<UserController>
        [HttpPost]
        public object Post([FromBody] User user)
        {
            db.Users.Add(user);
            return db.SaveChanges();
        }

        [HttpPut]
        public object Put(int id, User user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return Ok();
            }
            return BadRequest();
        }

        [HttpDelete]
        public object Delete(int id)
        {
            User user = db.Users.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            db.Users.Remove(user);
            db.SaveChanges();

            return Ok(user);
        }
    }
}
