﻿using Microsoft.AspNetCore.Mvc;
using Auth.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Auth.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private IJwtAuthenticationManager jwtAuthenticationManager;
        ApplicationDbContext db;
        public AuthController(IJwtAuthenticationManager jwtAuthenticationManager, ApplicationDbContext db)
        {
            this.jwtAuthenticationManager = jwtAuthenticationManager;
            this.db = db;
        }

        // POST api/<AuthsController>
        [HttpPost]
        public object Post([FromBody] User user)
        {
            int userTotal = db.Users
                            .Where(c=>c.Username == user.Username)
                            .Where(c=>c.Password == user.Password)
                            .Count();

            if(userTotal == 0) return Unauthorized();
            var token = jwtAuthenticationManager.Authenticate(user.Username);
            return Ok(token);
        }
    }
}
